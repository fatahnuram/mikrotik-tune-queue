#!/usr/bin/env bash

# check exported vars if exist
if [[ -z ${MIKROTIK_RULE_TYPE} ]] || [[ -z ${MIKROTIK_USER} ]] \
|| [[ -z ${MIKROTIK_ADDR} ]] || [[ -z ${MIKROTIK_ADDR_TYPE} ]] \
|| [[ -z ${MIKROTIK_MAX_UPLOAD} ]] || [[ -z ${MIKROTIK_MAX_DOWNLOAD} ]] \
|| [[ -z ${MIKROTIK_BURST_LIMIT_UPLOAD} ]] || [[ -z ${MIKROTIK_BURST_LIMIT_DOWNLOAD} ]] \
|| [[ -z ${MIKROTIK_BURST_THRESHOLD_UPLOAD} ]] || [[ -z ${MIKROTIK_BURST_THRESHOLD_DOWNLOAD} ]] \
|| [[ -z ${MIKROTIK_BURST_TIME_UPLOAD} ]] || [[ -z ${MIKROTIK_BURST_TIME_DOWNLOAD} ]] \
|| [[ -z ${MIKROTIK_LIMIT_AT_UPLOAD} ]] || [[ -z ${MIKROTIK_LIMIT_AT_DOWNLOAD} ]] \
|| [[ -z ${MIKROTIK_PRIORITY_UPLOAD} ]] || [[ -z ${MIKROTIK_PRIORITY_DOWNLOAD} ]]; then
    # missing expected exported variables
    echo
    echo "Looks like you don't source the conf yet!"
    echo "Please run ./configure.sh first!"
    echo "Aborting.."
    echo
    exit 1
fi

# check dependent exported vars if exist
if [[ ${MIKROTIK_ADDR_TYPE} == range ]]; then
    # check vars if exported
    if [[ -z ${MIKROTIK_IP_START} ]] || [[ -z ${MIKROTIK_IP_END} ]] \
    || [[ -z ${MIKROTIK_ID_START} ]] || [[ -z ${MIKROTIK_ID_END} ]]; then
        # missing expected exported variables
        echo
        echo "Looks like you don't source the conf correctly!"
        echo "Please run ./configure.sh again!"
        echo "Aborting.."
        echo
        exit 1
    fi
elif [[ ${MIKROTIK_ADDR_TYPE} == custom ]]; then
    # check vars if exported
    if [[ -z ${MIKROTIK_IP_TO_CONF} ]] || [[ -z ${MIKROTIK_ID_TO_CONF} ]]; then
        # missing expected exported variables
        echo
        echo "Looks like you don't source the conf correctly!"
        echo "Please run ./configure.sh again!"
        echo "Aborting.."
        echo
        exit 1
    fi
else
    # unknown error
    echo
    echo "Unknown error occured, aborting.."
    echo
    exit 1
fi

# predefined function to do remote command on mikrotik router
remote_command() {
    ssh ${MIKROTIK_USER}@${MIKROTIK_ADDR} "$@"
    echo "$@"
    echo
    sleep 2
}

# predefined function to build query and exec on mikrotik
exec_tune(){
    # check address type
    if [[ ${1} == range ]]; then
        # get loop count
        LOOP=$((${MIKROTIK_ID_END} - ${MIKROTIK_ID_START}))
        # loop
        for i in $(seq 0 ${LOOP}); do
            # get current ID
            current_id=$((${i} + ${MIKROTIK_ID_START}))
            current_ip=$((${i} + ${MIKROTIK_IP_START}))
            # prompt user
            echo
            echo "Processing for IP ${current_ip}.."
            # build query
            query="/queue simple"
            query="${query} set ${current_id}" # to-do: match with rule type (add, edit, del)
            query="${query} name=\"${MIKROTIK_RULE_PREFIX}${current_ip}${MIKROTIK_RULE_SUFFIX}\""
            query="${query} disabled=no" # enable rule
            query="${query} max-limit=${MIKROTIK_MAX_UPLOAD}/${MIKROTIK_MAX_DOWNLOAD}"
            query="${query} burst-limit=${MIKROTIK_BURST_LIMIT_UPLOAD}/${MIKROTIK_BURST_LIMIT_DOWNLOAD}"
            query="${query} burst-threshold=${MIKROTIK_BURST_THRESHOLD_UPLOAD}/${MIKROTIK_BURST_THRESHOLD_DOWNLOAD}"
            query="${query} burst-time=${MIKROTIK_BURST_TIME_UPLOAD}/${MIKROTIK_BURST_TIME_DOWNLOAD}"
            query="${query} limit-at=${MIKROTIK_LIMIT_AT_UPLOAD}/${MIKROTIK_LIMIT_AT_DOWNLOAD}"
            query="${query} priority=${MIKROTIK_PRIORITY_UPLOAD}/${MIKROTIK_PRIORITY_DOWNLOAD}"
    
            # optional variables
            if [[ ! -z ${MIKROTIK_COMMENT} ]]; then
                query="${query} comment=\"${MIKROTIK_COMMENT}\""
            else
                query="${query} comment=\"\"" # remove comment
            fi

            # exec
            remote_command ${query} &>> rules.log
        done
    elif [[ ${1} == custom ]]; then
        # parse input as array
        ARR_IP=(${MIKROTIK_IP_TO_CONF})
        ARR_ID=(${MIKROTIK_ID_TO_CONF})
        # loop
        for i in ${!ARR_IP[@]}; do
            # get current status
            current_id=${ARR_ID[${i}]}
            current_ip=${ARR_IP[${i}]}
            # prompt user
            echo
            echo "Processing for IP ${current_ip}.."
            # build query
            query="/queue simple"
            query="${query} set ${current_id}" # to-do: match with rule type (add, edit, del)
            query="${query} name=\"${MIKROTIK_RULE_PREFIX}${current_ip}${MIKROTIK_RULE_SUFFIX}\""
            query="${query} disabled=no" # enable rule
            query="${query} max-limit=${MIKROTIK_MAX_UPLOAD}/${MIKROTIK_MAX_DOWNLOAD}"
            query="${query} burst-limit=${MIKROTIK_BURST_LIMIT_UPLOAD}/${MIKROTIK_BURST_LIMIT_DOWNLOAD}"
            query="${query} burst-threshold=${MIKROTIK_BURST_THRESHOLD_UPLOAD}/${MIKROTIK_BURST_THRESHOLD_DOWNLOAD}"
            query="${query} burst-time=${MIKROTIK_BURST_TIME_UPLOAD}/${MIKROTIK_BURST_TIME_DOWNLOAD}"
            query="${query} limit-at=${MIKROTIK_LIMIT_AT_UPLOAD}/${MIKROTIK_LIMIT_AT_DOWNLOAD}"
            query="${query} priority=${MIKROTIK_PRIORITY_UPLOAD}/${MIKROTIK_PRIORITY_DOWNLOAD}"
    
            # optional variables
            if [[ ! -z ${MIKROTIK_COMMENT} ]]; then
                query="${query} comment=\"${MIKROTIK_COMMENT}\""
            else
                query="${query} comment=\"\"" # remove comment
            fi

            # exec
            remote_command ${query} &>> rules.log
        done
    else
        # error defining address type
        echo
        echo "Error occured when building queries! Aborting.."
        echo
        exit 1
    fi
}

# predefined function to cleanup log files
cleanup_log(){
    echo
    echo "Cleaning up log files.."
    echo
    rm -f *.tmp # cleanup all temp files
    mkdir -p log
    rm -f log/*
    mv *.log log/ # move all log files
}

# check args num
if [[ $# -gt 0 ]]; then
    # any args inserted
    echo
    echo "Don't need any arguments to run this! Aborting.."
    echo
    exit 1
fi

# show ssh credentials
echo
echo "Running SSH for ${MIKROTIK_USER}@${MIKROTIK_ADDR} .."

# print simple queue rules and prompt user
echo
echo "Getting current rules.."
echo
remote_command '/queue simple print' &> current_rules.log

# prepare log
echo "" > rules.log
# execute on mikrotik
exec_tune ${MIKROTIK_ADDR_TYPE}

echo
echo "Getting modified rules.."
echo
remote_command '/queue simple print' &> modified_rules.log

# cleanup by moving all log files
cleanup_log

# todo: cleanup vars after tuning queue

# prompt user when done
echo
echo "Done!"
echo "Logs and results are saved into log directory"
echo
echo "Don't forget to clean your leftover by running:"
echo "source cleanup.sh"
echo
