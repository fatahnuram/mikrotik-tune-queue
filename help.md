# Help

This page is for helping people understand all about this project.

## Variable terminologies

There are some terminologies used for variable input:

- **Configuration type**: type of rules that wanted to be configured (**add** new rules, **edit** existing, or **delete** existing rules).
- **User**: user for SSH through MikroTik to configure the router.
- **MikroTik IP address**: IP address of MikroTik router to configure. Note that right now, this input can only support address in the same subnet.
- **Start IP**: this is the last octet of first IP address to be configured by MikroTik. Note that you only need to give the last octet instead of the whole IP address.
- **End IP**: this is the last octet of last IP address to be configured by MikroTik. Note that you only need to give the last octet instead of the whole IP address.
- **Start ID**: this is the ID in MikroTik's simple queue rule that match the first IP given before. This variable is for matching between the queue rule ID with the IP address to be configured.
- **Rule name prefix**: rule names that will be added into created or edited rules as prefix.
- **Rule name suffix**: rule names that will be added into created or edited rules as suffix.
- **Comment**: comment of the rules that will be created or edited.