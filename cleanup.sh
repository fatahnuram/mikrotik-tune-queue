#!/usr/bin/env bash

# check args num
if [[ $# -gt 0 ]]; then
    # any args inserted
    echo
    echo "Don't need any arguments to run this! Aborting.."
    echo
    exit 1
fi

# prompt user
echo
echo "Cleaning up environment.."
# unset all variables
unset MIKROTIK_RULE_TYPE
unset MIKROTIK_USER
unset MIKROTIK_ADDR
unset MIKROTIK_ADDR_TYPE
unset MIKROTIK_IP_START
unset MIKROTIK_IP_END
unset MIKROTIK_ID_START
unset MIKROTIK_ID_END
unset MIKROTIK_IP_TO_CONF
unset MIKROTIK_ID_TO_CONF
unset MIKROTIK_MAX_UPLOAD
unset MIKROTIK_MAX_DOWNLOAD
unset MIKROTIK_BURST_LIMIT_UPLOAD
unset MIKROTIK_BURST_LIMIT_DOWNLOAD
unset MIKROTIK_BURST_THRESHOLD_UPLOAD
unset MIKROTIK_BURST_THRESHOLD_DOWNLOAD
unset MIKROTIK_BURST_TIME_UPLOAD
unset MIKROTIK_BURST_TIME_DOWNLOAD
unset MIKROTIK_LIMIT_AT_UPLOAD
unset MIKROTIK_LIMIT_AT_DOWNLOAD
unset MIKROTIK_PRIORITY_UPLOAD
unset MIKROTIK_PRIORITY_DOWNLOAD
unset MIKROTIK_RULE_PREFIX
unset MIKROTIK_RULE_SUFFIX
unset MIKROTIK_COMMENT

# prompt user
echo
echo "Done!"
echo
