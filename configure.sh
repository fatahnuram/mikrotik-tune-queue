#!/usr/bin/env bash

# predefined function to check if variable is present or not
var_check() {
    if [[ -z ${1} ]]; then
        echo
        echo "Missing value! Aborting.."
        echo
        exit 1
    fi
}

# predefined function to verify configuration type
verify_rule_type() {
    # todo: combine if rules when all supported
    if [[ ${1} == add ]]; then
        # add new configurations
        echo
        echo "Not yet supported! Quitting.."
        echo
        exit 0
    elif [[ ${1} == edit ]]; then
        # edit existing configurations
        # do nothing
        true
    elif [[ ${1} == del ]]; then
        # delete existing configurations
        echo
        echo "Not yet supported! Quitting.."
        echo
        exit 0
    else
        # value not in range
        echo
        echo "Value must be 'add', 'edit', or 'delete' !"
        echo "Aborting.."
        echo
        exit 1
    fi
}

# predefined function to verify addresses type
verify_address_type() {
    if [[ ${1} == range ]] || [[ ${1} == custom ]]; then
        # verified
        true
    else
        # value not in range
        echo
        echo "Value must be 'range' or 'custom' !"
        echo "Aborting.."
        echo
        exit 1
    fi
}

# predefined function to verify ip address
verify_ip_address() {
    if [[ ${1} =~ ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$ ]]; then
        # valid ip address
        true
    else
        # invalid ip address
        echo
        echo "IP address invalid! Aborting.."
        echo
        exit 1
    fi
}

# predefined function to verify last octet of ip address
verify_last_octet(){
    if [[ ${1} =~ ^([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$ ]]; then
        # valid last octet ip address
        true
    else
        # invalid last octet ip address
        echo
        echo "Value invalid! Must be between 0 and 255 !"
        echo "Aborting.."
        echo
        exit 1
    fi
}

# predefined function to verify ID number
verify_id_num(){
    if [[ ${1} =~ ^([0-9]|[1-9][0-9]|[1-9][0-9]{2})$ ]]; then
        # valid last octet ip address
        true
    else
        # invalid last octet ip address
        echo
        echo "Value invalid! Must be between 0 and 999 !"
        echo "Aborting.."
        echo
        exit 1
    fi
}

# predefined function to verify ip range
verify_ip_range(){
    # check if zero or minus value
    local CALCULATE=$((${1} - ${2}))
    if [[ ${CALCULATE} -lt 0 ]]; then
        # valid range
        true
    else
        # invalid range
        echo
        echo "Start IP must greater than End IP address! Aborting.."
        echo
        exit 1
    fi
}

# predefined function to verify queue priority
verify_priority(){
    if [[ ${1} =~ ^[1-8]$ ]]; then
        # valid priority
        true
    else
        # invalid priority
        echo
        echo "Value must between 1 and 8 !"
        echo "Aborting.."
        echo
        exit 1
    fi
}

# predefined function to verify burst time
verify_burst_time(){
    if [[ ${1} =~ ^([1-9]|[1-5][0-9])$ ]]; then
        # valid priority
        true
    else
        # invalid priority
        echo
        echo "Value must between 1 and 59 !"
        echo "Aborting.."
        echo
        exit 1
    fi
}

# predefined function to insert config type
insert_config_type(){
    printf "Configuration type (add, edit, del): "
    read RULE_TYPE
    var_check ${RULE_TYPE}
    verify_rule_type ${RULE_TYPE}
    printf "Address type (range, custom): "
    read ADDR_TYPE
    var_check ${ADDR_TYPE}
    verify_address_type ${ADDR_TYPE}
    echo
}

# predefined function to insert credentials
insert_credentials(){
    printf "User for login: "
    read USER
    var_check ${USER}
    printf "MikroTik IP address: "
    read ADDR
    var_check ${ADDR}
    verify_ip_address ${ADDR}
    echo
}

# predefined function to insert ip addresses to configure
insert_ip_target() {
    # check addresses type
    if [[ ${1} == range ]]; then
        # insert ip address range
        printf "Start IP (last octet): "
        read IP_START
        var_check ${IP_START}
        verify_last_octet ${IP_START}
        printf "End IP (last octet): "
        read IP_END
        var_check ${IP_END}
        verify_last_octet ${IP_END}
        verify_ip_range ${IP_START} ${IP_END}
        printf "Start ID (0-999, based on MikroTik simple queue rule): "
        read ID_START
        var_check ${ID_START}
        verify_id_num ${ID_START}
        # count last ID from IP given
        ID_END=$((${IP_END} - ${IP_START} + ${ID_START}))
        echo
    elif [[ ${1} == custom ]]; then
        # insert custom ip address with matching ip addresses
        echo "Insert IP address(es) to config (last octet, space separated):"
        read IP_TO_CONF
        var_check ${IP_TO_CONF}
        echo "Insert matching ID from IP above to config (space separated):"
        read ID_TO_CONF
        var_check ${ID_TO_CONF}
        echo
    else
        # value not in range
        echo
        echo "Value must be 'range' or 'custom' !"
        echo "Aborting.."
        echo
        exit 1
    fi
}

# predefined function to print target ip address for setting conf file
export_ip_target() {
    # export address type
    echo "export MIKROTIK_ADDR_TYPE=${ADDR_TYPE}" >> mikrotik.conf
    # check address type
    if [[ ${1} == range ]]; then
        echo "export MIKROTIK_IP_START=${IP_START}" >> mikrotik.conf
        echo "export MIKROTIK_IP_END=${IP_END}" >> mikrotik.conf
        echo "export MIKROTIK_ID_START=${ID_START}" >> mikrotik.conf
        echo "export MIKROTIK_ID_END=${ID_END}" >> mikrotik.conf
    elif [[ ${1} == custom ]]; then
        echo "export MIKROTIK_IP_TO_CONF=\"${IP_TO_CONF}\"" >> mikrotik.conf
        echo "export MIKROTIK_ID_TO_CONF=\"${ID_TO_CONF}\"" >> mikrotik.conf
    else
        # value not in range
        echo
        echo "Value must be 'range' or 'custom' !"
        echo "Aborting.."
        echo
        exit 1
    fi
}

# predefined function to insert configurations
insert_config(){
    # todo: regex validation
    echo "Permitted value:"
    echo "unlimited, 64k, 128k, 256k, 384k, 512k, 768k, 1M, 2M, 3M, 4M, 5M, 10M"
    echo
    printf "Upload max limit (choose one): "
    read MAX_UPLOAD
    var_check ${MAX_UPLOAD}
    printf "Download max limit (choose one): "
    read MAX_DOWNLOAD
    var_check ${MAX_DOWNLOAD}
    printf "Upload burst limit (choose one): "
    read BURST_LIMIT_UPLOAD
    var_check ${BURST_LIMIT_UPLOAD}
    printf "Download burst limit (choose one): "
    read BURST_LIMIT_DOWNLOAD
    var_check ${BURST_LIMIT_DOWNLOAD}
    printf "Upload burst threshold (choose one): "
    read BURST_THRESHOLD_UPLOAD
    var_check ${BURST_THRESHOLD_UPLOAD}
    printf "Download burst threshold (choose one): "
    read BURST_THRESHOLD_DOWNLOAD
    var_check ${BURST_THRESHOLD_DOWNLOAD}
    printf "Upload burst time (1-59): "
    read BURST_TIME_UPLOAD
    var_check ${BURST_TIME_UPLOAD}
    verify_burst_time ${BURST_TIME_UPLOAD}
    printf "Download burst time (1-59): "
    read BURST_TIME_DOWNLOAD
    var_check ${BURST_TIME_DOWNLOAD}
    verify_burst_time ${BURST_TIME_UPLOAD}
    printf "Upload limit at (choose one): "
    read LIMIT_AT_UPLOAD
    var_check ${LIMIT_AT_UPLOAD}
    printf "Download limit at (choose one): "
    read LIMIT_AT_DOWNLOAD
    var_check ${LIMIT_AT_DOWNLOAD}
    printf "Upload priority (1-8, smaller higher): "
    read PRIORITY_UPLOAD
    var_check ${PRIORITY_UPLOAD}
    verify_priority ${PRIORITY_UPLOAD}
    printf "Download priority (1-8, smaller higher): "
    read PRIORITY_DOWNLOAD
    var_check ${PRIORITY_DOWNLOAD}
    verify_priority ${PRIORITY_DOWNLOAD}
    echo
}

# predefined function to insert customized optional config
insert_config_optional(){
    printf "Rule name prefix (optional): "
    read RULE_PREFIX
    printf "Rule name suffix (optional): "
    read RULE_SUFFIX
    printf "Comment (optional): "
    read COMMENT
    echo
}

# predefined function to export all variables
export_all(){
    echo "export MIKROTIK_RULE_TYPE=${RULE_TYPE}" > mikrotik.conf
    echo "export MIKROTIK_USER=${USER}" >> mikrotik.conf
    echo "export MIKROTIK_ADDR=${ADDR}" >> mikrotik.conf
    export_ip_target ${ADDR_TYPE}
    echo "export MIKROTIK_MAX_UPLOAD=${MAX_UPLOAD}" >> mikrotik.conf
    echo "export MIKROTIK_MAX_DOWNLOAD=${MAX_DOWNLOAD}" >> mikrotik.conf
    echo "export MIKROTIK_BURST_LIMIT_UPLOAD=${BURST_LIMIT_UPLOAD}" >> mikrotik.conf
    echo "export MIKROTIK_BURST_LIMIT_DOWNLOAD=${BURST_LIMIT_DOWNLOAD}" >> mikrotik.conf
    echo "export MIKROTIK_BURST_THRESHOLD_UPLOAD=${BURST_THRESHOLD_UPLOAD}" >> mikrotik.conf
    echo "export MIKROTIK_BURST_THRESHOLD_DOWNLOAD=${BURST_THRESHOLD_DOWNLOAD}" >> mikrotik.conf
    echo "export MIKROTIK_BURST_TIME_UPLOAD=${BURST_TIME_UPLOAD}" >> mikrotik.conf
    echo "export MIKROTIK_BURST_TIME_DOWNLOAD=${BURST_TIME_DOWNLOAD}" >> mikrotik.conf
    echo "export MIKROTIK_LIMIT_AT_UPLOAD=${LIMIT_AT_UPLOAD}" >> mikrotik.conf
    echo "export MIKROTIK_LIMIT_AT_DOWNLOAD=${LIMIT_AT_DOWNLOAD}" >> mikrotik.conf
    echo "export MIKROTIK_PRIORITY_UPLOAD=${PRIORITY_UPLOAD}" >> mikrotik.conf
    echo "export MIKROTIK_PRIORITY_DOWNLOAD=${PRIORITY_DOWNLOAD}" >> mikrotik.conf
    # optional variables
    if [[ ! -z ${RULE_PREFIX} ]]; then
        echo "export MIKROTIK_RULE_PREFIX=\"${RULE_PREFIX}\"" >> mikrotik.conf
    fi
    if [[ ! -z ${RULE_SUFFIX} ]]; then
        echo "export MIKROTIK_RULE_SUFFIX=\"${RULE_SUFFIX}\"" >> mikrotik.conf
    fi
    if [[ ! -z ${COMMENT} ]]; then
        echo "export MIKROTIK_COMMENT=\"${COMMENT}\"" >> mikrotik.conf
    fi
}

# check args num
if [[ $# -gt 0 ]]; then
    # any args inserted
    echo
    echo "Don't need any arguments to run this! Aborting.."
    echo
    exit 1
fi

# get configuration type
insert_config_type

# get mikrotik credentials
insert_credentials

# get IP rules to be configured
insert_ip_target ${ADDR_TYPE}

# get expected configurations
insert_config

# get customized configuration names
insert_config_optional

# export all variables saved into file
export_all

# prompt user
echo
echo "Configuration saved! You can use them by running:"
echo "source mikrotik.conf"
echo
echo "And then run the script to configure your router:"
echo "./tune-queue.sh"
echo
