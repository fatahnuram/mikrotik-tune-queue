# mikrotik-tune-queue

Bash script for automating simple queue management for MikroTik via SSH

## Notes

This repository is now **working** (only for **edit** until now). However, this repository is still **in development phase**.

## Requirements

Requirements to be fulfilled for this script to be working:

- SSH port enabled on MikroTik
- User can login without password to MikroTik (using SSH key)

## How to run

Make sure the script executable

```bash
chmod +x *.sh
```

Configure script to match your network needs

```bash
./configure.sh
```

Export variables from script

```bash
source mikrotik.conf
```

Finally, run the script

```bash
./tune-queue.sh
```

Don't forget to clean up your workspace after tuning your router

```bash
source cleanup.sh
```

## Limitations

Some limitations for this script (until now):

- Only port 22 for SSH login (hopefully can support custom port later)
- Can only configure for IPs with prefix /24 (subnet mask 255.255.255.0)
- Only support for simple queue management in MikroTik
- Created rule is for per IP, not the whole network queue limit
- Until now only support for edit existing rule in MikroTik
- etc..

## Help

For help, you can click [this link](/help.md).

## LICENSE

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png "WTFPL")

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

Copyright (C) 2018 Fatah Nur Alam Majid <fatahnuram@gmail.com>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.